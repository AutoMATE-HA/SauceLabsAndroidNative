package com.incluit.automate.module.saucelabs.osnative.conf;


public interface Constants {

    static final String USER_TO_UPLOAD_APP_KEY = "testobjectApiKey";
    static final String PLATFORM_NAME_KEY = "platformName";
    static final String PLATFORM_VERSION_KEY = "platformVersion";
    static final String DEVICE_NAME_KEY = "deviceName";
    static final String APP_KEY_KEY = "testobject_app_id";
    static final String TC_NAME_KEY = "name";
    static final String APPIUM_VERSION_KEY = "appiumVersion";
    
    static final String PLATFORM_NAME_VALUE = "Android";
    static final String APPIUM_VERSION_DEFAULT_VALUE = "1.8.1";
    
    static final String APPIUM_AUTOMATION_NAME = "automationName";
    static final String APPIUM_AUTOMATION_NAME_VALUE = "uiautomator2";
    
    static final String SAUCE_LABS_URL = "https://us1.appium.testobject.com/wd/hub";
    
    static final int FIND_ELEMENT_TIME_OUT_DEFAULT_VALUE = 5; 
    
    
}
