package com.incluit.automate.module.saucelabs.osnative.conf;

public interface PropertiesKeys {

    static final String APPIUM_VERSION_KEY = "appium.version";
    static final String AUTHETICATION_KEY = "sauce.labs.authetication.key";
    static final String APP_KEY = "app.key";
    static final String ANDROID_PLATFORM_VERSION_KEY = "android.version";
    static final String ANDROID_DEVICE_NAME_KEY = "android.device.name";
    
    static final String FIND_ELEMENT_TIME_OUT_KEY = "find.element.timeout";
    
}
