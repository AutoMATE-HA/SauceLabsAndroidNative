package com.incluit.automate.module.saucelabs.osnative.agent;

import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.InvalidParameterException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteTouchScreen;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.collect.ImmutableMap;
import com.incluit.automate.core.agent.Agent;
import com.incluit.automate.core.exceptions.AgentException;
import com.incluit.automate.core.exceptions.PropertyException;
import com.incluit.automate.core.runner.JBehaveRunner;
import com.incluit.automate.core.structures.FlawedTimeUnit;
import com.incluit.automate.core.structures.ScrollDirection;
import com.incluit.automate.core.structures.SwipeDirection;
import com.incluit.automate.core.utils.ReadProperty;
import com.incluit.automate.module.saucelabs.osnative.conf.Constants;
import com.incluit.automate.module.saucelabs.osnative.conf.PropertiesKeys;

public class AgentImpl implements Agent {

    /**
     * Logger object
     */
    private static Logger log = Logger.getLogger(AgentImpl.class.getName());

    /**
     * Android driver
     */
    private AndroidDriver<MobileElement> driver;

    private int FIND_ELEMENT_TIME_OUT_SECONDS = 0;

    /**
     * Driver Wait
     */
    private WebDriverWait wait;

    /**
     * Touch interactions objects
     */
    private RemoteTouchScreen touch;
    private MultiTouchAction multiTouch;

    @Override
    public void start(String applicationName) throws AgentException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        try {
            FIND_ELEMENT_TIME_OUT_SECONDS = ReadProperty.getPropertyInt(
                    PropertiesKeys.FIND_ELEMENT_TIME_OUT_KEY,
                    Constants.FIND_ELEMENT_TIME_OUT_DEFAULT_VALUE);
            capabilities.setCapability(Constants.USER_TO_UPLOAD_APP_KEY,
                    ReadProperty.getProperty(PropertiesKeys.AUTHETICATION_KEY));
            capabilities.setCapability(Constants.PLATFORM_NAME_KEY,
                    Constants.PLATFORM_NAME_VALUE);
            capabilities.setCapability(Constants.APPIUM_AUTOMATION_NAME, Constants.APPIUM_AUTOMATION_NAME_VALUE);
            capabilities
                    .setCapability(
                            Constants.PLATFORM_VERSION_KEY,
                            ReadProperty
                                    .getProperty(PropertiesKeys.ANDROID_PLATFORM_VERSION_KEY));
            capabilities.setCapability(Constants.DEVICE_NAME_KEY, ReadProperty
                    .getProperty(PropertiesKeys.ANDROID_DEVICE_NAME_KEY));
            capabilities.setCapability(Constants.APP_KEY_KEY,
                    ReadProperty.getProperty(PropertiesKeys.APP_KEY));
//            capabilities.setCapability("idleTimeout", 1000);
            capabilities.setCapability(Constants.TC_NAME_KEY,
                    JBehaveRunner.getScriptName());
            capabilities.setCapability(Constants.APPIUM_VERSION_KEY,
                    ReadProperty.getProperty(PropertiesKeys.APPIUM_VERSION_KEY,
                            Constants.APPIUM_VERSION_DEFAULT_VALUE));
        } catch (PropertyException e) {
            throw new AgentException(e.getMessage(), this);
        }
        try {
            driver = new AndroidDriver<MobileElement>(new java.net.URL(
                    Constants.SAUCE_LABS_URL), capabilities);
        } catch (MalformedURLException e) {
        }

        log.info("The connection to Sauce Labs was successful");
    }

    @Override
    public void waitForVanish(Object element, FlawedTimeUnit timeout)
            throws AgentException {
        WebDriverWait auxWait = new WebDriverWait(driver, timeout.toSeconds());
        try {
            auxWait.until(ExpectedConditions
                    .invisibilityOfElementLocated((By) element));
        } catch (TimeoutException e) {
            // TODO: add log message
            e.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.harriague.automate.core.agent.Agent#writeInElement(java.lang.Object,
     * java.lang.String)
     */
    public void writeInElement(Object element, String text)
            throws AgentException {
        // TODO: add log message
        WebElement webElement = new WebDriverWait(driver,
                FIND_ELEMENT_TIME_OUT_SECONDS).until(ExpectedConditions
                .elementToBeClickable((By) element));
        webElement.clear();
        webElement.sendKeys(text);
    }

    @Override
    public void takeScreenshot() throws AgentException {
        File srcFiler = getScreenshotAs(OutputType.FILE);
        DateFormat dateFormat = new SimpleDateFormat("MM_dd_HH_mm_ss");
        String folder = com.incluit.automate.core.conf.Constants.SCREENSHOT_FOLDER_PATH;
        String fileName = "agentScreenshot_"
                + dateFormat.format(new Date().getTime());
        try {
            FileUtils.copyFile(srcFiler, new File(folder + "/" + fileName
                    + ".png"));
        } catch (Exception e) {
        }
    }

    @Override
    public void highlightElement(Object element) throws AgentException {
        // in Android can't be implemented
    }

    @Override
    public boolean checkElementIsDisplayed(Object element)
            throws AgentException {
        // TODO: add log message
        WebElement aux = null;
        try {
            aux = wait.until(ExpectedConditions
                    .visibilityOfElementLocated((By) element));
        } catch (TimeoutException e) {
        }
        return aux != null;
    }

    @Override
    public boolean checkElementIsDisplayed(Object element,
            FlawedTimeUnit timeout) {
        // TODO: add log message
        WebElement aux = null;
        WebDriverWait auxWait = new WebDriverWait(driver, timeout.toSeconds());
        try {
            aux = auxWait.until(ExpectedConditions
                    .visibilityOfElementLocated((By) element));
        } catch (Exception e) {
        }
        return aux != null;
    }

    public boolean checkAllElementAreDisplay(FlawedTimeUnit timeout,
            By xpaths[]) {
        final WebDriverWait auxWait = new WebDriverWait(driver,
                timeout.toSeconds());
        try {
            return Arrays.stream(xpaths).allMatch(
                    by -> auxWait.until(ExpectedConditions
                            .visibilityOfElementLocated((by))) != null);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkAnyElementIsDisplay(FlawedTimeUnit timeout, By xpaths[]) {
        final WebDriverWait auxWait = new WebDriverWait(driver,
                timeout.toSeconds());
        try {
            return Arrays.stream(xpaths).anyMatch(
                    by -> auxWait.until(ExpectedConditions
                            .visibilityOfElementLocated((by))) != null);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkAllSubElementsAreDisplay(FlawedTimeUnit timeout,
            WebElement parent, By xpaths[]) {
        final WebDriverWait auxWait = new WebDriverWait(driver,
                timeout.toSeconds());
        try {
            return Arrays.stream(xpaths)
                    .allMatch(
                            by -> auxWait.until(ExpectedConditions
                                    .presenceOfNestedElementLocatedBy(parent,
                                            by)) != null);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkAnySubElementsIsDisplay(FlawedTimeUnit timeout,
            WebElement parent, By xpaths[]) {
        final WebDriverWait auxWait = new WebDriverWait(driver,
                timeout.toSeconds());
        try {
            return Arrays.stream(xpaths)
                    .anyMatch(
                            by -> auxWait.until(ExpectedConditions
                                    .presenceOfNestedElementLocatedBy(parent,
                                            by)) != null);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void pressKey(Object key) throws AgentException {
        // To see available key codes refer to:
        // "http://developer.android.com/reference/android/view/KeyEvent.html"
        // TODO: add log message
        driver.pressKeyCode((int) key);
    }

    protected static ImmutableMap<String, Object> getCommandImmutableMap(
            String param, Object value) {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put(param, value);
        return builder.build();
    }

    @Override
    public void longPressKey(Object key) {
        driver.longPressKeyCode((int) key);
    }

    public void longPressWithTime(Object by, FlawedTimeUnit duration)
            throws AgentException, InterruptedException {
        WebElement elem = findElement(by);
        int miliseconds = (int) duration.toMilliseconds();
        new TouchAction(driver).longPress(elem, miliseconds)
                .waitAction(miliseconds).release().perform();
    }

    @Override
    public void click(Object element) throws AgentException {
        // TODO: add log message
        findElement((By) element).click();
    }

    @Override
    public void back() throws AgentException {
        driver.navigate().back();
    }

    @Override
    public void rightClick(Object element) throws AgentException {
        // TODO Auto-generated method stub
    }

    @Override
    public void scroll(ScrollDirection direction, int amount)
            throws AgentException {
        // TODO: add log message
        if (direction.equals(ScrollDirection.DOWN)) {
            amount *= -1;
        }
        touch.scroll(0, amount);
    }

    /**
     * Get object to start building a custom gesture.
     *
     * @return TouchAction instance
     */
    public TouchAction getCustomGestureBuilder() {
        return new TouchAction(driver);
    }

    /**
     * Builds and performs a customized gesture
     *
     * @param customActions
     *            List<TouchAction>
     * @throws AgentException
     */
    public void executeCustomGesture(List<Object> customActions)
            throws AgentException {
        for (Object action : customActions) {
            try {
                multiTouch.add((TouchAction) action);
            } catch (ClassCastException e) {
                log.info("A '" + TouchAction.class + "' object was expected"
                        + " and '" + action.getClass() + "' was provided");
                throw new AgentException(e.getMessage(), e, this);
            }
        }
        multiTouch.perform();
    }

    @Override
    public void scrollIntoView(Object elem) throws AgentException {
        int numberOfScolls = 0;
        WebElement element;
        final FlawedTimeUnit timeout = FlawedTimeUnit.seconds(3);
        while (numberOfScolls < 20) {
            try {
                element = quickFindElement((By) elem, timeout);
            } catch (Exception e) {
                element = null;
            }
            if (element != null) {
                break;
            }
            this.swipe(SwipeDirection.DOWN);
            numberOfScolls++;
        }
    }

    /**
     * Search an element for the timeout specified
     *
     * @param by
     *            to element
     * @param timeout
     *            search time out
     * @return WebElement
     * @throws AgentException
     */
    public WebElement quickFindElement(By by, FlawedTimeUnit timeout)
            throws AgentException {
        driver.manage().timeouts()
                .implicitlyWait(timeout.toSeconds(), TimeUnit.SECONDS);
        WebElement element;
        try {
            element = driver.findElement(by);
        } catch (Exception e) {
            element = null;
        }
        driver.manage()
                .timeouts()
                .implicitlyWait(FIND_ELEMENT_TIME_OUT_SECONDS, TimeUnit.SECONDS);
        return element;
    }

    /**
     * Find a element in page
     *
     * @param by
     *            to element
     * @return WebElement
     * @throws AgentException
     */
    public WebElement findElement(Object by) throws AgentException {
        return new WebDriverWait(driver, FIND_ELEMENT_TIME_OUT_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated((By) by));
    }

    /*
     * (non-Javadoc)80
     * 
     * @see com.harriague.automate.core.agent.Agent#close()
     */
    @Override
    public void close() {
        driver.quit();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.harriague.automate.core.agent.Agent#navegateTo(java.lang.String)
     */
    public void navigateTo(String url) throws AgentException {
        if (url.equals("back")) {
            driver.navigate().back();
        }
    }

    @Override
    public String getTextValue(Object element) throws AgentException {
        String aux = null;
        aux = findElement((By) element).getText();
        log.info("Text Value: '" + aux + "'");
        return aux;
    }

    @Override
    public String getValue(Object element, String attribute)
            throws AgentException {
        String aux = null;
        aux = findElement((By) element).getAttribute(attribute);
        return aux;
    }

    @Override
    public List<Map<String, Object>> getGrid(Object id) throws AgentException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void selectInCombobox(Object xpath, Object value)
            throws AgentException {
        boolean isSelected = false;
        Select select = new Select(quickFindElement((By) xpath,
                FlawedTimeUnit.seconds(FIND_ELEMENT_TIME_OUT_SECONDS)));
        for (int i = 0; i < select.getOptions().size(); i++) {
            if (select.getOptions().get(i).getText().contains((String) value)) {
                select.selectByIndex(i);
                isSelected = true;
                break;
            }
        }
        if (isSelected) {
            log.info("The option '" + ((String) value) + "' is selected.");
        } else {
            throw new AgentException("The option '" + ((String) value)
                    + "' not exist", this);
        }
    }

    @Override
    public void maximizeWindows() throws AgentException {
        // TODO Auto-generated method stub

    }

    @Override
    public Object hover(Object xpath) throws AgentException {
        WebElement we = findElement((By) xpath);
        TouchAction touchAction = new TouchAction(driver);
        // we.click();
        touchAction.press(we).perform();
        touchAction.press(we).perform();
        return we;
    }

    @Override
    public void waitForVanish(Object element) throws AgentException {
        try {
            wait.until(ExpectedConditions
                    .invisibilityOfElementLocated((By) element));
        } catch (TimeoutException e) {
            // TODO: add log message
            e.printStackTrace();
        }
    }

    public void swipe(SwipeDirection direction) {
        int xstart = 250, ystart = 250;
        int xOffSet = 250, yOffSet = 250;
        log.info("going " + direction);
        switch (direction) {
        case LEFT:
            xOffSet = 500;
            break;
        case UP:
            yOffSet = 500;
            break;
        case DOWN:
            yOffSet = 1;
            break;
        case RIGHT:
            xOffSet = 1;
            break;
        case DIAGONAL_RIGHT_TOP:
            xstart = 350;
            xOffSet = 200;
            ystart = 200;
            yOffSet = 600;
            break;
        case DIAGONAL_RIGHT_BOTTOM:
            xstart = 350;
            xOffSet = 200;
            ystart = 600;
            yOffSet = 200;
            break;
        case DIAGONAL_LEFT_TOP:
            xstart = 200;
            xOffSet = 950;
            ystart = 200;
            yOffSet = 600;
            break;
        case DIAGONAL_LEFT_BOTTOM:
            xstart = 200;
            xOffSet = 950;
            ystart = 600;
            yOffSet = 200;
            break;
        default:
            throw new InvalidParameterException("Direction "
                    + direction.toString() + " s not valid");

        }
        driver.swipe(xstart, ystart, xOffSet, yOffSet, 600);
    }

    @Override
    public void swipe(int startx, int starty, int endx, int endy,
            FlawedTimeUnit duration) {
        driver.swipe(startx, starty, endx, endy, (int) duration.toMilliseconds());
    }

    public void zoomOut(int x, int y) {
        driver.zoom(x, y);
    }

    public void zoomIn(int x, int y) {
        driver.pinch(x, y);
    }

    @Override
    public List<MobileElement> findElements(Object by) {
        return driver.findElements((By) by);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType)
            throws WebDriverException {
        return driver.getScreenshotAs(outputType);
    }

    public void leavePressTouch(int x, int y) {
        new TouchAction(driver).press(x, y).perform();
    }

    /**
     * Convenience method that takes a screenshot of the device and returns a
     * BufferedImage for further processing.
     *
     * @return screenshot from the device as BufferedImage
     */
    public BufferedImage takeScreenshotAsBuffer() {
        log.info("taking screenshot");
        File scrFile = getScreenshotAs(OutputType.FILE);
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(scrFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bufferedImage;
    }

    /**
     * Gets the window height
     */
    public int getHeight() {
        return driver.manage().window().getSize().getHeight();
    }

    /**
     * Gets the window width
     */
    public int getWidth() {
        return driver.manage().window().getSize().getWidth();
    }

    /*
     * Convenience method for tapping a position on the screen
     * 
     * @param fingers number of finger to perform this action
     * 
     * @param positionX the location in X where tap
     * 
     * @param positionY the location in Y where tap
     * 
     * @param duration time in mills for the duration of the tap
     */
    @Override
    public void clickInTheScreen(int fingers, int positionX, int positionY,
            FlawedTimeUnit duration) {
        driver.tap(fingers, positionX, positionY,
                (int) duration.toMilliseconds());
    }

    /**
     * This method starts an activity
     *
     * @param appPackage
     * @param appActivity
     * @param appWaitPackage
     * @param appWaitActivity
     * @param stopApp
     */
    public void startActivity(String appPackage, String appActivity,
            String appWaitPackage, String appWaitActivity, boolean stopApp) {
        this.driver.startActivity(appPackage, appActivity, appWaitPackage,
                appWaitActivity, stopApp);
    }

    /**
     * This method starts an activity
     *
     * @param appPackage
     * @param appActivity
     * @param appWaitPackage
     * @param appWaitActivity
     */
    public void startActivity(String appPackage, String appActivity,
            String appWaitPackage, String appWaitActivity) {
        this.driver.startActivity(appPackage, appActivity, appWaitPackage,
                appWaitActivity);
    }

    /**
     * This method resets the current app.
     */
    public void resetApp() {
        this.driver.resetApp();
    }

    /**
     * This method gets the current activity
     *
     * @return
     */
    public String getCurrentActivity() {
        return this.driver.currentActivity();
    }

    /**
     * This method returns true or false either an app is installed or not
     *
     * @param bundleId
     *            - bundleId of the app
     * @return
     */
    public boolean isAppInstalled(String bundleId) {
        return this.driver.isAppInstalled(bundleId);
    }

    @Override
    public Process sendCommand(String command) throws AgentException {
        return null;
    }

    @Override
    public Process waitForCommand(String command) throws AgentException {
        return null;
    }

}
